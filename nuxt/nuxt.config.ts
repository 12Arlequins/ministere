import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  build: {
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {},
        },
      },
    },
  },
  css: [
    "~/assets/css/tailwind.css",
    '~/assets/css/fonts.css',
  ],
})

// useHead({
//   link: [
//     {
//       rel: 'preconnect',
//       href: 'https://fonts.googleapis.com'
//     },
//     {
//       rel: 'stylesheet',
//       href: 'https://fonts.googleapis.com/css2?family=Alatsi&family=Cinzel+Decorative:wght@700&display=swap',
//       crossorigin: ''
//     },
//     {
//       rel: 'script',
//       src: 'https://cdn.tailwindcss.com',
//       crossorigin: ''
//     }
//   ]
// })
